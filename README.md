MaskedField
==============

Is a component to filter inputs on the client using a mask on input field.

This component may have two values with respect to the mask and ask if it is valid.

* getValue\(\) get only characters (no separators).
* getMaskText\(\) get all text with separators.
* isValid\(\) check if complete.


# How to use
* \# allow only numbers
* A allow only capital letters
* a allow only no capital letters
* B allow capital and no capital letters
* C allow capital letters, no capital letters and numbers

## Separators
Can use all of those character to separate
### \/,\*\.\-:\(\)\{\}=\+

You can add a white space for separator

# Example

\#\#\#-\#\#\#
## Result: 123-123

AA\*AA
## Result: FV*VB
