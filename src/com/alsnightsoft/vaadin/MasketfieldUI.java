package com.alsnightsoft.vaadin;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@Theme("masketfield")
public class MasketfieldUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = MasketfieldUI.class, widgetset = "com.alsnightsoft.vaadin.MasketfieldWidgetset")
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		final VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		layout.setMargin(true);
		setContent(layout);

		final MaskedField maskedField = new MaskedField("#-###", "Example Masked Field Required");
		maskedField.setRequired(true);
		maskedField.setValue("8888");
		maskedField.setValue("8-888");
		
		final MaskedField maskedField2 = new MaskedField("#-###-###-####", "Example Masked Field Read only");
		maskedField2.setReadOnly(true);
		
		final MaskedField maskedField3 = new MaskedField("#-###-###-####", "Normal");
		maskedField3.setSizeFull();

		final TextField tfMask = new TextField("Mask");
		final TextField tfData = new TextField("Data");

		final Button btnChangeMask = new Button("Asing mask");
		btnChangeMask.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				maskedField.setMask(tfMask.getValue());
			}
		});
		
		final Button btnResult = new Button("Get Result");
		btnResult.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				tfData.setValue(maskedField.getValue());
				System.out.println("Get value: " + maskedField.getValue());
				System.out.println("Get Mask Text: " + maskedField.getMaskText());
				System.out.println("Get Mask: " + maskedField.getMask());
				System.out.println("Is valid? " + maskedField.isValid());
				tfData.setValue(maskedField.getMaskText());
			}
		});

		layout.addComponent(maskedField);
		layout.addComponent(maskedField2);
		layout.addComponent(maskedField3);
		layout.addComponent(tfMask);
		layout.addComponent(btnChangeMask);
		layout.addComponent(btnResult);
		layout.addComponent(tfData);
	}

}