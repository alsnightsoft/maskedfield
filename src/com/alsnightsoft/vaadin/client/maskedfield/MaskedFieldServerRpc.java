package com.alsnightsoft.vaadin.client.maskedfield;

import com.vaadin.shared.communication.ServerRpc;

public interface MaskedFieldServerRpc extends ServerRpc {

	public void changeUnparsedText(String unparsedText);

	public void changeIncludePlaceHolderInValue(boolean includePlaceHolderInValue);

	public void changeMask(String mask);

	public void changeText(String text);
}
