package com.alsnightsoft.vaadin.client.maskedfield;

import com.alsnightsoft.vaadin.MaskedField;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.RpcProxy;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.textfield.TextFieldConnector;
import com.vaadin.shared.ui.Connect;

@Connect(MaskedField.class)
public class MaskedFieldConnector extends TextFieldConnector {

	private static final long serialVersionUID = -1761889019913481173L;

	MaskedFieldServerRpc rpc = RpcProxy.create(MaskedFieldServerRpc.class, this);

	public MaskedFieldConnector() {
		registerRpc(MaskedFieldClientRpc.class, new MaskedFieldClientRpc() {

			private static final long serialVersionUID = -5676309381177942904L;

			@Override
			public void setMaskText(String text) {
				getState().text = getState().translatedMask;
				getState().unparsedText = "";
				addText(text == null ? "" : text);
			}

			@Override
			public void setMask(String mask) {
				getState().mask = mask;
				getState().text = "";
				getState().unparsedText = "";
				getState().regexSegments.clear();
				translateMask();
			}

			@Override
			public void setIncludePlaceHolderInValue(boolean includePlaceHolderInValue) {
				getState().includePlaceHolderInValue = includePlaceHolderInValue;
			}
		});
		getWidget().addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				int keyCode = event.getNativeKeyCode();
				if (event.isControlKeyDown() && event.getNativeKeyCode() == KeyCodes.KEY_V) {
					onPaste();
				} else if (keyCode == KeyCodes.KEY_BACKSPACE) {
					removeLastCharacter();
					getWidget().cancelKey();
				} else if (keyCode == KeyCodes.KEY_DELETE) {
					getWidget().cancelKey();
				}
			}
		});
		getWidget().addFocusHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				refreshText();
			}
		});
		getWidget().addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				validateAndAddCharCode(event.getCharCode());
				refreshText();
				getWidget().cancelKey();
			}
		});
	}

	@Override
	protected Widget createWidget() {
		return GWT.create(MaskedFieldWidget.class);
	}

	@Override
	public MaskedFieldWidget getWidget() {
		return (MaskedFieldWidget) super.getWidget();
	}

	@Override
	public MaskedFieldState getState() {
		return (MaskedFieldState) super.getState();
	}

	@Override
	public void onStateChanged(StateChangeEvent stateChangeEvent) {
		super.onStateChanged(stateChangeEvent);
	}

	private void addCharacter(Character ch) {
		getState().text = getState().text.replaceFirst("[" + getState().PLACEHOLDER_CHAR + "]", String.valueOf(ch));
	}

	private void addText(String text) {
		for (char ch : text.toCharArray()) {
			validateAndAddCharCode(ch);
		}
		refreshText();
	}

	private void formatAndRefresh() {
		getState().text = getState().translatedMask;
		for (char ch : getState().unparsedText.toCharArray()) {
			addCharacter(ch);
		}
		refreshText();
	}

	private void refreshText() {
		getWidget().setValue(getState().text);
		int caretIndex = getState().text.indexOf(getState().PLACEHOLDER_CHAR);
		if (caretIndex == -1) {
			caretIndex = getState().text.length();
		}
		getWidget().setSelectionRange(caretIndex, 0);
		rpc.changeText(getState().text);
		rpc.changeMask(getState().mask);
		rpc.changeUnparsedText(getState().unparsedText);
	}

	private void translateMask() {
		getState().translatedMask = "";
		for (Character ch : getState().mask.toCharArray()) {
			switch (ch) {
			case '#':
				getState().regexSegments.add("[0-9]{1}");
				getState().translatedMask += getState().PLACEHOLDER_CHAR;
				break;
			case 'A':
				getState().regexSegments.add("[A-Z]{1}");
				getState().translatedMask += getState().PLACEHOLDER_CHAR;
				break;

			case 'a':
				getState().regexSegments.add("[a-z]{1}");
				getState().translatedMask += getState().PLACEHOLDER_CHAR;
				break;

			case 'B':
				getState().regexSegments.add("[A-Za-z]{1}");
				getState().translatedMask += getState().PLACEHOLDER_CHAR;
				break;

			case 'C':
				getState().regexSegments.add("[A-Za-z0-9]{1}");
				getState().translatedMask += getState().PLACEHOLDER_CHAR;
				break;
			case '/':
			case ',':
			case '*':
			case '.':
			case '-':
			case ':':
			case '(':
			case ')':
			case '{':
			case '}':
			case '=':
			case '+':
			case ' ':
				getState().translatedMask += ch;
				break;
			default:
			}
		}
		getState().text = getState().translatedMask;
		getWidget().setText(getState().text);
		refreshText();
	}

	private void validateAndAddCharCode(Character ch) {
		if (getState().unparsedText.length() + 1 <= getState().regexSegments.size()
				&& ch.toString().matches(getState().regexSegments.get(getState().unparsedText.length()))) {
			getState().unparsedText += ch;
			addCharacter(ch);
		}
	}

	private void removeLastCharacter() {
		if (!getState().unparsedText.isEmpty()) {
			getState().unparsedText = getState().unparsedText.substring(0, getState().unparsedText.length() - 1);
		}
		formatAndRefresh();
	}

	private void onPaste() {
		getWidget().setFocus(true);
		Scheduler.get().scheduleFixedPeriod(new Scheduler.RepeatingCommand() {
			@Override
			public boolean execute() {
				String value = getWidget().getValue();
				getWidget().setValue("");
				addText(value);
				getWidget().setFocus(true);
				return false;
			}
		}, getState().PASTE_DELAY);
	}

}
