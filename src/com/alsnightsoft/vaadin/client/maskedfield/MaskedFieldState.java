package com.alsnightsoft.vaadin.client.maskedfield;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.shared.ui.textfield.AbstractTextFieldState;

public class MaskedFieldState extends AbstractTextFieldState {

	private static final long serialVersionUID = -2638560842863769565L;

	public String PLACEHOLDER_CHAR = "_";

	public int PASTE_DELAY = 10;

	public List<String> regexSegments = new ArrayList<>();
	
	public List<String> extraRegexSegments = new ArrayList<>();

	public String translatedMask;

	public String unparsedText = "";

	public boolean includePlaceHolderInValue = true;

	public String mask;

}