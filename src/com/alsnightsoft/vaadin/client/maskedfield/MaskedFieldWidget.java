package com.alsnightsoft.vaadin.client.maskedfield;

import com.vaadin.client.ui.VTextField;

public class MaskedFieldWidget extends VTextField {

	public static final String CLASSNAME = "v-maskedfield";

	public MaskedFieldWidget() {
		addStyleName(CLASSNAME);
	}

}