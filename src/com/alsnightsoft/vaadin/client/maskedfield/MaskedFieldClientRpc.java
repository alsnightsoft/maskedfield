package com.alsnightsoft.vaadin.client.maskedfield;

import com.vaadin.shared.communication.ClientRpc;

public interface MaskedFieldClientRpc extends ClientRpc {

	public void setMask(String mask);

	public void setIncludePlaceHolderInValue(boolean includePlaceHolderInValue);

	public void setMaskText(String text);

}